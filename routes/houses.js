const express = require('express');
const db = require('../db/db');

const router = express.Router();

router.get('/', (req, res, next) => {
    const houses = db.get('houses');
    const quiz = db.get('quiz');
    res.json({ status: 'OK', data: houses, quiz });
});

module.exports = router;
