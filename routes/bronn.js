const express = require('express');
const { validate } = require('jsonschema');
const db = require('../db/db');

const router = express.Router();

router.get('/', (req, res, next) => {
    const bronn = db.get('bronn');
    res.json({ status: 'OK', data: bronn });
});

router.get('/:id', ((req, res, next) => {
    const { id } = req.params;
    const data = db.get('bronn').get('celebrities').find((celebrity) => String(celebrity.id) === id);
    res.json({ status: 'OK', data, message: 'Здесь пока пусто :(' });
}));

router.post('/', ((req, res, next) => {
    const { body } = req;

    const taskSchema = {
        type: 'object',
        properties: {
            name: { type: 'string' },
            about: { type: 'string' }
        },
        required: ['name'],
        additionalProperties: false,
    };

    const validationResult = validate(body, taskSchema);

    if (!validationResult.valid) {
        return next(new Error('INVALID_JSON_OR_API_FORMAT'));
    }

    const newPerson = { id: Math.floor(Math.random() * 100000).toString(), name: body.name, about: body.about };

    try {
        db.get('bronn').get('celebrities').push(newPerson).write();
    } catch (error) {
        throw new Error(error);
    }

    res.json({ status: 'OK', newPerson });
}));

router.delete('/:id', ((req, res, next) => {
    const { id } = req.params;
    db.get('bronn').get('celebrities').remove({ id }).write();

    res.json({ status: 'OK' });
}));

module.exports = router;