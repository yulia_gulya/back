const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');

const PORT = 8000;
const HOST = 'localhost';

const app = express();
const housesRoutes = require('./routes/houses');
const lannisterRoutes = require('./routes/lannister');
const baratheonRoutes = require('./routes/baratheon');
const tullyRoutes = require('./routes/tully');
const arrynRoutes = require('./routes/arryn');
const greyjoyRoutes = require('./routes/greyjoy');
const bronnRoutes = require('./routes/bronn');
const martellRoutes = require('./routes/martell');
const starkRoutes = require('./routes/stark');

app.use(morgan('combined'));
app.use(bodyParser.json());

app.use('/lannister', lannisterRoutes);
app.use('/baratheon', baratheonRoutes);
app.use('/tully', tullyRoutes);
app.use('/arryn', arrynRoutes);
app.use('/greyjoy', greyjoyRoutes);
app.use('/bronn', bronnRoutes);
app.use('/martell', martellRoutes);
app.use('/stark', starkRoutes);

app.get('/', housesRoutes);

app.use((err, req, res, next) => {
    const { message } = err;
    res.json({ status: 'ERROR', message });
});

app.listen(PORT, () => console.log(`listening on http://${HOST}:${PORT}`));
