Для запуска проекта нужно:

1. Склонировать репозитории:
   git clone https://yulia_gulya@bitbucket.org/yulia_gulya/back.git
   git clone https://yulia_gulya@bitbucket.org/yulia_gulya/front.git

2. npm install

3. npm start (back - localhost:8000, front - localhost:3000)